#ifndef H_WRITER
#define H_WRITER

#include "error_handling.h"
#include "image.h"
#include <stdio.h>
/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_CANNOT_OPEN_FILE,
    WRITE_IMAGE_ISNT_VALID,
    WRITE_CANNOT_WRITE,
    WRITE_ERROR,
};
enum writer_status {
    WRITE_NICE = 0,
    WRITE_FAILED_CANT_OPEN,
    WRITE_FAILED_CANT_WRITE,
    WRITE_FAILED_CANT_CLOSE,
};

//static uint32_t image_size(struct image *img);
//static struct bmp_header create_header(struct image *img);
enum write_status to_bmp(FILE *out, struct image *img);
void write_image_bmp(const char *name, struct image *img, struct error_handler *eh);
#endif
