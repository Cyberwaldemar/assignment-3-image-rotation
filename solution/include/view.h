#ifndef H_VIEW
#define H_VIEW
#include "error_handling.h"
#include "file_io.h"
#include "image.h"
#include "reader.h"
#include "rotator.h"
#include "writer.h"

int view(const char *name_in, const char *name_out, int32_t angle);
#endif
