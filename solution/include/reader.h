#ifndef H_READER
#define H_READER

#include "error_handling.h"
#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FAILED_SUCCESSFULLY,
    READ_INVALID_FILE,
};
enum header_check{
    HEADER_INVALID=0,
    HEADER_VALID,
};
enum reader_status{
    READ_NICE = 0,
    READ_FAILED,
};
enum read_status from_bmp(FILE *in, struct image *img);
void read_image_bmp(const char *name, struct image *img, struct error_handler *eh);
#endif
