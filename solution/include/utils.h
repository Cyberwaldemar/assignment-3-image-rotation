#ifndef H_UTILS
#define H_UTILS
#include "image.h"
#include <stdint.h>
#include <stdlib.h>
struct pixel *calc_image_line_pointer(struct pixel *ptr, uint32_t width, uint32_t row);
uint32_t calc_padding(uint32_t w);

void *malloc_2(size_t size, int16_t timeout);
#endif
