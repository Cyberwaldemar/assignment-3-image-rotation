#ifndef H_FILE_IO
#define H_FILE_IO
#include "error_handling.h"
#include <stdio.h>

enum file_status{
    FILE_OK = 0,
    FILE_CANNOT_BE_OPEN,
    FILE_CANNOT_BE_CLOSED,
};
enum open_mode{
    OPEN_NOT_OPEN=0,
    OPEN_READ,
    OPEN_WRITE,
    OPEN_APPEND,
    OPEN_READ_BINARY,
    OPEN_WRITE_BINARY,
    OPEN_APPEND_BINARY,
    OPEN_WRITE_READ,
    OPEN_NEW_WRITE_READ,
    OPEN_APPEND_OR_NEW_WRITE_READ,
    OPEN_WRITE_READ_BINARY,
    OPEN_NEW_WRITE_READ_BINARY,
    OPEN_APPEND_OR_NEW_WRITE_READ_BINARY,
};
enum file_status open_file(const char *path, FILE **fp, enum open_mode mode);
enum file_status close_file(FILE *fp);
void print_file_status(error_code_type s);
#endif
