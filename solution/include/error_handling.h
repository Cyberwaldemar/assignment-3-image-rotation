#ifndef H_ERROR_HANDLING
#define H_ERROR_HANDLING

#include <stddef.h>

#include <stdint.h>

#define OOM_ERR 42

typedef int32_t error_code_type;
typedef void (*handler_lambda)(error_code_type);

struct error_handler{
	error_code_type total;
	error_code_type* errors;
	size_t n;
	size_t i_err;
	size_t i_hnd;
	handler_lambda *handler;
	void (*init)(struct error_handler*, size_t);
	void (*add_handler)(struct error_handler*, handler_lambda);
	void (*add_error)(struct error_handler *, error_code_type);
	void (*run)(const struct error_handler*);
	void (*kill)(struct error_handler *);
};
void malloc_check(void *ptr);
struct error_handler error_handler_new(void);

#endif
