#ifndef H_ROTATOR
#define H_ROTATOR
#include "image.h"
#include <stdint.h>

enum rotate_status{
	ANGLE_VALID=0,
	ANGLE_INVALID,
	CANT_ROTATE,
};
enum angle_standarts{
	FULL_SPIN = 360,
	HALF_SPIN = 180,
	QUATER_SPIN = 90,
};
//static struct image rotate90(const struct image source);
enum rotate_status rotate_image(struct image *img, int32_t angle);
void print_rotate_status(enum rotate_status s);
#endif
