#if defined _MSC_VER
#define __attribute__(x)
#endif

#ifndef H_IMAGE
#define H_IMAGE

#include <stdint.h>

#pragma pack(push, 1)
struct __attribute__((packed)) pixel{
	uint8_t b, g, r;
};
#pragma pack(pop)
struct image {
	uint32_t width, height;
	struct pixel *data;
};

struct image create_image(uint32_t w, uint32_t h);
void clear_image(struct image *img);
#endif
