#ifndef H_BMP
#define H_BMP
#include <stdbool.h>
#include <stdint.h>

#if defined _MSC_VER
#define __attribute__(x)
#endif

#define BMP_HEADER 0x4D42
#define HEADER_SIZE 54
#define BMP_HEADER_SIZE 40
#define BMP_BI_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_BI_COMPRESSION 0
#define BMP_PEELS 2834
#define BMP_COLORS_UNUSED 0


#pragma pack(push, 1)
struct __attribute__((packed))  bmp_header{
    // FILE HEADER (14 bytes)
    uint16_t bfType; //Signature
    uint32_t bfileSize; //
    uint32_t bfReserved;
    uint32_t bOffBits;
    // BITMAP HEADER V3 (40 bytes)
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
    // bitmask and other else 
};
#pragma pack(pop)

bool check_header(const struct bmp_header *header);
#endif
