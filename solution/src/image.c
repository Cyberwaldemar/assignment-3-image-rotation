#include "error_handling.h"
#include "image.h"
#include "utils.h"
#include <stdlib.h>
struct image create_image(uint32_t w, uint32_t h){
    struct image result={
        .width = w,
        .height = h,
        .data = malloc_2(w * h * (uint32_t)sizeof(struct pixel), 3)
    };
    malloc_check(result.data);
    return result;
}

void clear_image(struct image *img){ 
    if(img != NULL){
        free(img->data);
        img->data=NULL;
    }
}
