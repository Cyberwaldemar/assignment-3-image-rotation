#include "error_handling.h"

#include "file_io.h"
#include <stdint.h>
enum file_status open_file(const char *path, FILE **fp, enum open_mode mode){
	
	switch(mode){
		case OPEN_READ:
			*fp = fopen(path, "r");
			break;
		case OPEN_WRITE:
			*fp = fopen(path, "w");
			break;
		case OPEN_APPEND:
			*fp = fopen(path, "a");
			break;
		case OPEN_READ_BINARY:
			*fp = fopen(path, "rb");
			break;
		case OPEN_WRITE_BINARY:
			*fp = fopen(path, "wb");
			break;
		case OPEN_APPEND_BINARY:
			*fp = fopen(path, "ab");
			break;

		case OPEN_WRITE_READ:
			*fp = fopen(path, "r+");
			break;
		case OPEN_NEW_WRITE_READ:
			*fp = fopen(path, "w+");
			break;
		case OPEN_APPEND_OR_NEW_WRITE_READ:
			*fp = fopen(path, "a+");
			break;
		case OPEN_WRITE_READ_BINARY:
			*fp = fopen(path, "r+b");
			break;
		case OPEN_NEW_WRITE_READ_BINARY:
			*fp = fopen(path, "w+b");
			break;
		case OPEN_APPEND_OR_NEW_WRITE_READ_BINARY:
			*fp = fopen(path, "a+b");
			break;
		case OPEN_NOT_OPEN:
		default:
			return FILE_CANNOT_BE_OPEN;
	}

	if(*fp == NULL){
		return FILE_CANNOT_BE_OPEN;
	}
	return FILE_OK;
}

enum file_status close_file(FILE *fp){
	if(fclose(fp)){
		return FILE_CANNOT_BE_CLOSED;
	}
	return FILE_OK;
}
void print_file_status(error_code_type s){
	char *error_codes[] = {
		[FILE_OK] = "OK",
		[FILE_CANNOT_BE_OPEN] = "FILE CANNOT BE OPEN",
		[FILE_CANNOT_BE_CLOSED] = "FILE CANNOT BE CLOSED",
	};
	if(!s) return;
	fprintf(stderr, "ERROR WHILE FILE IO: %s \n", error_codes[s]);
}
