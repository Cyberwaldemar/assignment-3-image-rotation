#include "error_handling.h"

#include <stdio.h>
#include <stdlib.h>
void malloc_check(void *ptr){
	if(ptr == NULL){
		fprintf(stderr, "Out Of Memory, exiting...");
		exit(OOM_ERR);
	}
}
static void error_handler_add_handler(struct error_handler *h, handler_lambda new_handler){
	if(h->i_hnd < h->n){
		(h->handler[h->i_hnd]) = new_handler;
		h->i_hnd = h->i_hnd + 1;
	}
}
static void error_handler_add_error(struct error_handler *h, error_code_type error){
	if(h->i_err < h->n){
		h->errors[h->i_err] = error;
		h->i_err = h->i_err + 1;
		if(error){
			h->total = 1;
		}
	}
}
static void error_handler_run(const struct error_handler *eh){
	for(size_t ie = 0, ih = 0; ie < eh->i_err && ih < eh->i_hnd; ie++, ih++){
		(eh->handler[ih])(eh->errors[ie]);
	}
}
static void error_handler_kill(struct error_handler *eh){
	free(eh->handler);
	free(eh->errors);
}
static void error_handler_init_constructor(struct error_handler *a, size_t errors_value){
	if(errors_value==0){
		errors_value=1;
	}
	struct error_handler result = error_handler_new();
	result.n = errors_value;
	result.i_err = 0;
	result.i_hnd = 0;
	result.total = 0;
	*a = result;
	a->errors = malloc(errors_value*sizeof(error_code_type));
	a->handler = malloc(errors_value * sizeof(handler_lambda));
	malloc_check(a->errors);
	malloc_check(a->handler);
	
}

struct error_handler error_handler_new(void){
	return (struct error_handler){
		.n = 0,
		.i_err = 0,
		.i_hnd = 0,
		.total=0,
		.errors = NULL,
		.handler = NULL,
		.init = error_handler_init_constructor,
		.add_handler = error_handler_add_handler,
		.add_error = error_handler_add_error,
		.run = error_handler_run,
		.kill = error_handler_kill,
	};
}
