#include "bmp.h"

bool check_header(const struct bmp_header *header){
	//Supports :clown: only BITMAP V3, with 40+12 size header
	if(header->biSize != BMP_HEADER_SIZE)
		return false;
	if(header->biWidth == 0)
		return false;
	if(header->biHeight == 0)
		return false;
	// Only 1 is allowed
	if(header->biPlanes != BMP_BI_PLANES)
		return false;
	// Only 24-bit color
	if(header->biBitCount != BMP_BIT_COUNT)
		return false;
	// No comperession
	if(header->biCompression != BMP_BI_COMPRESSION){
		return false;
	}
	return true;
}

