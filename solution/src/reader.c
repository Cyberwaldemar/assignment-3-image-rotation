#include "bmp.h"
#include "file_io.h"

#include "reader.h"

#include "utils.h"
#include <stdint.h>
enum read_status from_bmp(FILE *in, struct image *img){
	struct bmp_header header;
	
	if(fread(&header, sizeof(struct bmp_header), 1, in) !=1)
		return READ_INVALID_FILE;
	if(header.bfType!= BMP_HEADER){
		return READ_INVALID_SIGNATURE;
	}
	if(!check_header(&header)){
		return READ_INVALID_HEADER;
	}
	if(fseek(in, header.bOffBits, SEEK_SET)!=0){
		return READ_INVALID_BITS;
	}
	uint32_t h, w;
	h=header.biHeight;
	w=header.biWidth;
	uint32_t padding=calc_padding(w);

	*img = create_image(w, h);

	for(uint32_t i = 0; i < h; i++){
		if(
			fread(calc_image_line_pointer(img->data, w, i),
			  sizeof(struct pixel) * w, 1, in) !=1
			)
			  return READ_FAILED_SUCCESSFULLY;
		if(
			fseek(in, padding, SEEK_CUR)
			)
			  return READ_FAILED_SUCCESSFULLY;
	}
	return READ_OK;
}

void read_image_bmp(const char *name, struct image *img, struct error_handler *eh){
	FILE *fp = NULL;
	enum file_status fls_open = open_file(name, &fp, OPEN_READ_BINARY);
	eh->add_error(eh, fls_open);
	if(eh->total)
		return;
	enum read_status rs = from_bmp(fp, img);
	eh->add_error(eh, rs);
	if(eh->total)
		return;
	enum file_status fls_close = close_file(fp);
	eh->add_error(eh, fls_close);
}

