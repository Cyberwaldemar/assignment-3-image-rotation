#include "image.h"
//#include "reader.h"
#include "rotator.h"
#include "view.h"
//#include "writer.h"
#include <stdio.h>
#include <stdlib.h>
#define ERROR_WRONG_ARGUMENTS_NUMBER 1
#define ERROR_CANT_READ 2
#define ERROR_CANT_ROTATE 3
#define ERROR_CANT_WRITE 4

#define NEED_ARGUMENTS 4
#define ARG_NAME_INPUT 1
#define ARG_NAME_OUTPUT 2
#define ARG_ANGLE 3
int main( int argc, char** argv ) {
    if(argc != NEED_ARGUMENTS){
        fprintf(stderr, "ERROR: WRONG ARGUMENTS NUMBER\n");
        return ERROR_WRONG_ARGUMENTS_NUMBER;
    }
    
    char *name_in = argv[ARG_NAME_INPUT];
    char *name_out = argv[ARG_NAME_OUTPUT];
    int32_t angle = (int32_t)atoi(argv[ARG_ANGLE]);
    return view(name_in, name_out, angle);
}
