#include "rotator.h"
#include "utils.h"
#include <stdio.h>
void print_rotate_status(enum rotate_status s){
	char *error_codes[] = {
		[ANGLE_VALID] = "OK",
		[ANGLE_INVALID] = "ANGLE INVALID",
		[CANT_ROTATE] = "IMAGE CANNOT BE ROTATED",
	};
	if(!s) return;
	fprintf(stderr, "ERROR WHILE ROTATING: %s \n", error_codes[s]);
}

static inline int32_t angle_convert(int32_t angle){
	return ((angle % FULL_SPIN) + FULL_SPIN) % FULL_SPIN;
}
static struct image rotate90(const struct image source){
	struct image new_image = create_image(source.height, source.width);
	for(uint32_t i = 0; i < source.height; i++){
		for(uint32_t j = 1; j <= source.width; j++){
			new_image.data[j * source.height - i - 1] = source.data[i * (source.width) + j - 1];
		}
	}
	return new_image;
}

enum rotate_status rotate_image(struct image *img, int32_t angle){
	
	angle = angle_convert(-angle);
	if(angle % QUATER_SPIN != 0){
		fprintf(stderr, "error: invalid angle\n");
		return ANGLE_INVALID;
	}
	for(int32_t a = 0; a < angle; a += QUATER_SPIN){
		struct image rotated = {.height=0, .width=0, .data=NULL};
		rotated = rotate90(*img);
		clear_image(img);
		*img=rotated;
	}
	return ANGLE_VALID;
}

