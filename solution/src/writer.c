#include "bmp.h"
#include "file_io.h"
#include "utils.h"

#include "writer.h"

#include <stdint.h>
static void create_padding_arr(uint8_t *const padding_arr, const uint8_t padding){
	for(size_t i = 0; i < padding; i++){
		padding_arr[i] = 0;
	}
}
static uint32_t image_size(const struct image *img){
	return img->height *
		(img->width* (uint32_t)(sizeof(struct pixel))
		+ calc_padding(img->width));
}
static struct bmp_header create_header(const struct image *img){
	return (struct bmp_header){
		.bfType = BMP_HEADER,
		.bfileSize= sizeof(struct bmp_header) + image_size(img),
		.bOffBits = sizeof (struct bmp_header),
		.biSize = BMP_HEADER_SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = BMP_BI_PLANES,
		.biBitCount = BMP_BIT_COUNT,
		.biCompression = BMP_BI_COMPRESSION,
		.biSizeImage = image_size(img),
		.biXPelsPerMeter = BMP_PEELS,
		.biYPelsPerMeter = BMP_PEELS,
		.biClrUsed = BMP_COLORS_UNUSED,
		.biClrImportant = BMP_COLORS_UNUSED,
	};
}

enum write_status to_bmp(FILE *out, struct image *img){
	struct bmp_header header = create_header(img);
	if(fwrite(&header, sizeof(header), 1, out) != 1)
		return WRITE_CANNOT_WRITE;
	if(img->height * img->width == 0){
		return WRITE_IMAGE_ISNT_VALID;
	}
	uint32_t padding = calc_padding(img->width);
	uint8_t padding_array[4];
	create_padding_arr(padding_array, 4);
	for(uint32_t h = 0; h < img->height; h++){
		if(fwrite(img->data + img->width * h,
		  img->width * sizeof(struct pixel), 1, out) < 1)
			return WRITE_ERROR;
		if(fwrite(padding_array, padding, 1, out) < 1)
			return WRITE_ERROR;
		
	}
	return WRITE_OK;
}

void write_image_bmp(const char *name, struct image *img, struct error_handler *eh){
	FILE *fp = NULL;
	enum file_status fls_open = open_file(name, &fp, OPEN_WRITE_BINARY);
	eh->add_error(eh, fls_open);
	if(eh->total)
		return;
	enum write_status ws = to_bmp(fp, img);

	eh->add_error(eh, ws);
	if(eh->total)
		return;
	enum file_status fls_close = close_file(fp);
	eh->add_error(eh, fls_close);
	if(eh->total)
		return;
}

