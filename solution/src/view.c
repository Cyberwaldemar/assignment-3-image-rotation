#include "view.h"
#define ERROR_CANT_READ 2
#define ERROR_CANT_ROTATE 3
#define ERROR_CANT_WRITE 4
void print_read_status(error_code_type s){
    char *error_codes[] = {
        [READ_OK] = "OK",
        [READ_INVALID_SIGNATURE] = "INVALID FILE SIGNATURE",
        [READ_INVALID_BITS] = "INVALID BITS",
        [READ_INVALID_HEADER] = "INVALID HEADER",
        [READ_FAILED_SUCCESSFULLY] = "READ FAILED SUCCESSFULLY",
        [READ_INVALID_FILE] = "INVALID FILE",
    };
    if(!s) return;

    fprintf(stderr, "ERROR WHILE READING: %s \n", error_codes[s]);
}
void print_write_status(error_code_type s){
    char *error_codes[] = {
        [WRITE_OK] = "OK",
        [WRITE_CANNOT_OPEN_FILE] = "CANNOT OPEN FILE",
        [WRITE_IMAGE_ISNT_VALID] = "INVALID IMAGE",
        [WRITE_CANNOT_WRITE] = "CANNOT WRITE",
        [WRITE_ERROR] = "WRITE FAILED SUCCESSFULLY",
    };
    if(!s) return;
    fprintf(stderr, "ERROR WHILE WRITING: %s \n", error_codes[s]);
}
int view(const char *name_in, const char *name_out, int32_t angle){
    struct image img = {.width = 0, .height = 0, .data = NULL};
    struct error_handler err_read = error_handler_new();

    err_read.init(&err_read, 3);
    err_read.add_handler(&err_read, print_file_status);
    err_read.add_handler(&err_read, print_read_status);
    err_read.add_handler(&err_read, print_file_status);

    read_image_bmp(name_in, &img, &err_read);
    if(err_read.total){
        err_read.run(&err_read);
        clear_image(&img);
        err_read.kill(&err_read);
        return ERROR_CANT_READ;
    }
    err_read.kill(&err_read);
    
    enum rotate_status st = rotate_image(&img, angle);
    print_rotate_status(st);
    if(st){
        clear_image(&img);
        return ERROR_CANT_ROTATE;
    }

    struct error_handler err_write = error_handler_new();
    err_write.init(&err_write, 3);
    err_write.add_handler(&err_write, print_file_status);
    err_write.add_handler(&err_write, print_write_status);
    err_write.add_handler(&err_write, print_file_status);
    write_image_bmp(name_out, &img, &err_write);
    if(err_write.total){
        err_read.run(&err_read);
        clear_image(&img);
        err_write.kill(&err_write);
        return ERROR_CANT_WRITE;
    }
    clear_image(&img);
    err_write.kill(&err_write);
    return 0;
}
